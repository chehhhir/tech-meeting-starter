#!/usr/bin/env bash
set -e

cd "$(dirname "$0")"

PROJECT_DIR=$(dirname "$(pwd)")/tech-meet

source utils.bash

echo -e "${START_EMOJI}${YELLOW} Cloning repositories: ${NC} \n"
clone_if_not_exists "tech-meeting-server" "https://gitlab.com/chehhhir/tech-meeting-server.git" "$PROJECT_DIR"
clone_if_not_exists "tech-meeting-client" "https://gitlab.com/chehhhir/tech-meeting-client.git" "$PROJECT_DIR"

echo -e "\n${YELLOW}Checking Minikube installation:${NC}"
if ! command -v minikube >/dev/null 2>&1; then
  echo -e "${RED}${ERROR_EMOJI} Minikube not found. Please install Minikube on your machine.${NC}"
  exit 1
else
  echo -e "${GREEN}${SUCCESS_EMOJI} Minikube is installed.${NC}"
  echo -e "${YELLOW}Checking Minikube status:${NC}"

  if minikube status >/dev/null 2>&1; then
    echo -e "${GREEN}${SUCCESS_EMOJI} Minikube is already running.${NC}"
  else
    echo -e "${YELLOW}Starting Minikube:${NC}"

    if ! minikube start; then
      echo -e "${RED}${ERROR_EMOJI} Failed to start Minikube.${NC}"
      exit 1
    else
      echo -e "\n${GREEN}${SUCCESS_EMOJI} Minikube started.${NC}"
    fi
  fi
fi
