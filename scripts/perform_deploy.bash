#!/usr/bin/env bash
set -e

cd "$(dirname "$0")"

PROJECT_DIR=$(dirname "$(pwd)")/tech-meet

source utils.bash

for repo_name in "tech-meeting-server" "tech-meeting-client"; do
  repo_dir="$PROJECT_DIR/$repo_name"
  ci_file="$repo_dir/.gitlab-ci.yml"
  new_tag=$(generate_random_tags)

  if [[ -f "$ci_file" ]]; then
    echo "Updating tags in gitlab-ci.yml for repository: $repo_name"
    update_tags "$repo_name" "$ci_file" "$new_tag"

    if [ -f "$repo_dir/docker/.runner_client" ]; then
      echo "Updating runner tag for: $repo_name"
      sed -i "s#RUNNER_TAG_LIST=.*#RUNNER_TAG_LIST=$new_tag#" "$repo_dir/docker/.runner_client"
    fi

    if [ -f "$repo_dir/docker/.runner_server" ]; then
      echo "Updating runner tag for: $repo_name"
      sed -i "s#RUNNER_TAG_LIST=.*#RUNNER_TAG_LIST=$new_tag#" "$repo_dir/docker/.runner_server"
    fi

  else
    echo "gitlab-ci.yml not found in repository: $repo_name"
  fi
done

echo -e "\n${YELLOW}Starting runners:${NC}"

start_runner "$PROJECT_DIR/tech-meeting-server/docker/docker-compose-local.yaml" "tech-meeting" "runner_server"
start_runner "$PROJECT_DIR/tech-meeting-client/docker/docker-compose-local.yaml" "tech-meeting" "runner_client"

echo -e "\n${YELLOW}Listing Docker containers:${NC}"
if ! docker ps -a; then
  echo -e "${RED}${ERROR_EMOJI} Failed to list Docker containers.${NC}"
  exit 1
fi
